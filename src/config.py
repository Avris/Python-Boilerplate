from dotenv import load_dotenv
import os
from pathlib import Path

load_dotenv(dotenv_path=os.getenv('DOTENV'))

env = os.getenv('APP_ENV')

projectDir = Path(__file__).parent.parent.absolute()
