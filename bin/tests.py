import os
from pathlib import Path
os.environ['DOTENV'] = str(Path(__file__).parent.parent.absolute() / 'testcases' / 'run' / '.env')
import unittest
from testcases.foo import *

if __name__ == '__main__':
    unittest.main(verbosity=2)
