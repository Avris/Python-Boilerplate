from argparse import ArgumentParser
from src.foo import Foo


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('value')
    args = parser.parse_args()

    foo = Foo()
    print(foo.bar(int(args.value)))
