# Avris Boilerplate

Boilerplate code for a Python3 project

## Prerequisites

 - Python 3.6
 - PIP

## Installation

    mkdir project_name
    cd project_name
    git init
    git pull git@gitlab.com:Avris/Python-Boilerplate.git

	py -m venv venv
	export PYTHONPATH=$(pwd)          # or windows: SET PYTHONPATH=%cd%
	source venv/Scripts/activate      # or windows: venv\Scripts\activate.bat
    export PYTHONIOENCODING='utf_8'   # or windows: SET PYTHONIOENCODING=utf_8 
	cp .env.dist .env                 # and edit it according to your needs 
	pip install -r requirements.txt

## Run

    py bin/run.py 8
    
## Tests

    py bin/tests.py
