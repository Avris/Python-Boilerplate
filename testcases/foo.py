import unittest
from src.foo import Foo


class TestFoo(unittest.TestCase):
    def setUp(self):
        self.foo = Foo()

    def test_bar(self):
        self.assertEqual(9, self.foo.bar(8))
        self.assertEqual(8, self.foo.bar(7))
